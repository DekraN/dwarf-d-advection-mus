#!/bin/bash
#BSUB -n 4
#BSUB -e ERRFILE # error file name in which %J is replaced by the job ID
#BSUB -o OUTFILE # output file name in which %J is replaced by the job IDt
#BSUB -J dwarf_mus # job name
#BSUB -x 
#BSUB -R span[ptile=16]
#BSUB -q poe_short


# Launcher

MPIEXEC=mpiexec #mpirun_Impi5
export PROC_X=2
export PROC_Y=2
export JPI=100
export JPJ=100
export JPK=30
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK ) ))
export TIMING=true
export MPI_TIMING=true
export TEST_MODE=true
export WITH_INDEX=true
export USE_NETCDF=true
export OUT_FILENAME="out"
export OUT_FILENAME_NETCDF="out_scenario_2.nc" # "out_scenario_1.nc"
export IN_FILENAME_NETCDF="init_scenario_2.nc" # "in_scenario_1.nc"
export LOG_PREFIX="out" #"prova/out_"
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
