#!/bin/bash
#BSUB -n 1024
#BSUB -e ERRFILE # error file name in which %J is replaced by the job ID
#BSUB -o OUTFILE # output file name in which %J is replaced by the job IDt
#BSUB -J dwarf_mus_SCAL # job name
#BSUB -q poe_long


# Launcher

MPIEXEC=/usr/bin/mpirun #mpirun_Impi5
export KJPT=2
export TIMING=true
export MPI_TIMING=true
export TEST_MODE=false
export LOG_PREFIX="prova/out_"


#####################################################################################################################################
# SCALABILITY TESTS
#####################################################################################################################################

mkdir -p SCAL


#####################################################################################################################################
# G8 Domain size: 2240 x 1500 x 31
#####################################################################################################################################

export JPI=2240
export JPJ=1500
export JPK=31
mkdir -p SCAL/G8

export PROC_X=8
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P32/out_"
mkdir -p SCAL/G8/P32
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=8
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P64/out_"
mkdir -p SCAL/G8/P64
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P128/out_"
mkdir -p SCAL/G8/P128
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P256/out_"
mkdir -p SCAL/G8/P256
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P512/out_"
mkdir -p SCAL/G8/P512
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=32
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G8/P1024/out_"
mkdir -p SCAL/G8/P1024
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

#####################################################################################################################################
# G4 Domain size: 560 x 375 x 31
#####################################################################################################################################

export JPI=560
export JPJ=375
export JPK=31
mkdir -p SCAL/G4

export PROC_X=8
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P32/out_"
mkdir -p SCAL/G4/P32
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=8
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P64/out_"
mkdir -p SCAL/G4/P64
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P128/out_"
mkdir -p SCAL/G4/P128
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P256/out_"
mkdir -p SCAL/G4/P256
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P512/out_"
mkdir -p SCAL/G4/P512
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=32
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G4/P1024/out_"
mkdir -p SCAL/G4/P1024
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

#####################################################################################################################################
# G1 Domain size: 70 x 46 x 19
#####################################################################################################################################

export JPI=70
export JPJ=46
export JPK=19
mkdir -p SCAL/G1

export PROC_X=8
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P32/out_"
mkdir -p SCAL/G1/P32
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=8
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P64/out_"
mkdir -p SCAL/G1/P64
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=8
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P128/out_"
mkdir -p SCAL/G1/P128
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=16
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P256/out_"
mkdir -p SCAL/G1/P256
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=16
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P512/out_"
mkdir -p SCAL/G1/P512
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=32
export PROC_Y=32
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SCAL/G1/P1024/out_"
mkdir -p SCAL/G1/P1024
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

