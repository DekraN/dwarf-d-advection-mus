#!/bin/bash
#BSUB -n 16
#BSUB -e ERRFILE # error file name in which %J is replaced by the job ID
#BSUB -o OUTFILE # output file name in which %J is replaced by the job IDt
#BSUB -J dwarf_mus_SNOD # job name
#BSUB -x 
#BSUB -R span[ptile=16]
#BSUB -q poe_long


# Launcher

MPIEXEC=/usr/bin/mpirun #mpirun_Impi5
export KJPT=2
export TIMING=true
export MPI_TIMING=true
export TEST_MODE=false
export LOG_PREFIX="prova/out_"

#####################################################################################################################################
# SINGLE NODE TESTS
#####################################################################################################################################

mkdir -p SNOD

#####################################################################################################################################
# G8 Domain size: 2240 x 1500 x 31
#####################################################################################################################################

export JPI=2240
export JPJ=1500
export JPK=31
mkdir -p SNOD/G8

export PROC_X=1
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G8/P1/out_"
mkdir -p SNOD/G8/P1
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G8/P2/out_"
mkdir -p SNOD/G8/P2
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G8/P4/out_"
mkdir -p SNOD/G8/P4
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G8/P8/out_"
mkdir -p SNOD/G8/P8
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G8/P16/out_"
mkdir -p SNOD/G8/P16
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

#####################################################################################################################################
# G4 Domain size: 560 x 375 x 31
#####################################################################################################################################

export JPI=560
export JPJ=375
export JPK=31
mkdir -p SNOD/G4

export PROC_X=1
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G4/P1/out_"
mkdir -p SNOD/G4/P1
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G4/P2/out_"
mkdir -p SNOD/G4/P2
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G4/P4/out_"
mkdir -p SNOD/G4/P4
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G4/P8/out_"
mkdir -p SNOD/G4/P8
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G4/P16/out_"
mkdir -p SNOD/G4/P16
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

#####################################################################################################################################
# G1 Domain size: 70 x 46 x 19
#####################################################################################################################################

export JPI=70
export JPJ=46
export JPK=19
mkdir -p SNOD/G1

export PROC_X=1
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G1/P1/out_"
mkdir -p SNOD/G1/P1
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=1
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G1/P2/out_"
mkdir -p SNOD/G1/P2
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=2
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G1/P4/out_"
mkdir -p SNOD/G1/P4
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=2
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G1/P8/out_"
mkdir -p SNOD/G1/P8
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
wait

export PROC_X=4
export PROC_Y=4
JPI_Local=$(( (JPI + PROC_X -3) / PROC_X +2 ))
JPJ_Local=$(( (JPJ + PROC_Y -3) / PROC_Y +2 ))
export KIT_END=$((2200000000 / (JPI_Local * JPJ_Local * JPK * KJPT) ))
export LOG_PREFIX="SNOD/G1/P16/out_"
mkdir -p SNOD/G1/P16
$MPIEXEC -np $((PROC_X * PROC_Y)) dwarf.sh
