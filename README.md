Dwarf-D-advection-MUS
==========================
Contact: Marco Chiarelli (marco.chiarelli@cmcc.it) 
or Italo Epicoco (italo.epicoco@unisalento.it)

Dwarf-D-advection-MUS implements and solves the tracer
advection equation. It has been extracted from the CMCC's
GLOB16 NEMO-based ocean model.
Based on previous discretization adopted, several semi-discrete
space forms of the tracer equations are available depending
on the vertical coordinate used and on the physics used.
In this model, various advection scheme are implemented, and one
can select a scheme according to the choice made
in space and time interpolation to define the value of the tracer
at the velocity points based: this dwarf implements the MUSCL one
(Monotone Upstream Scheme for Conservative Laws), where the tracer
at velocity points is evaluated assuming a linear tracer variation
between two T -points.

The code is written in Fortran 90 (F90)
and it has been tested using the following compilers:
   
    gfortran 8.2.0
    ifort 15.0.3

Prototypes available
--------------------
- prototype1: first implementation of the MUSCL scheme

NOTE: add any new prototype above with a brief description.


Third Party Requirements
------------------------
Requirements to compile this dwarf:
   
    CMake: for use and installation see http://www.cmake.org/
    ecbuild: CMake based tools by ECMWF
    MPI: distributed memory parallelisation

Recommended:
   
    PAPI - Performance Application Programming Interface
           See : http://icl.utk.edu/papi/
	   Download at: https://bitbucket.org/icl/papi/src/master/
    Perf Regions: It depends on PAPI above
	   See : https://github.com/schreiberx/perf_regions/


Dependency:
-------------------------
- Having ECBuild system installed alongside the dwarf directory;
- Having Fortran compiler (*gfortran*, for example), and MPI development suite (*OpenMPI, MPICH*, eg.) installed;
- For performance benchmarking with Perf Regions, PAPI library is necessary, resp. (`sudo apt install papi-tools`), then (`sudo apt install libpapi-dev`), then you can find Perf Regions at: https://github.com/schreiberx/perf_regions/

Download and Installation
-------------------------
- `git clone https://DekraN@bitbucket.org/DekraN/dwarf-d-advection-mus.git`
- Create a directory named *build* in the dwarf root directory, resp. `mkdir -p build && cd build`
- Type `FC=mpif90 cmake ../` (you can use whichever Fortran MPI compiler wrapper you want, not necessarily *mpif90*, for example *mpiifort*) for CMake to prepare the make environment;
- Then, `make clean && make` for compile;

Running and testing
-------------------
- Run `ctest -VV` to launch the *ctest*'s test fixtures.

